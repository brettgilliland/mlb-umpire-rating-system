# MLB Umpire Rating System

A model created to evaluate and rate each MLB umpire on their pitch calling accuracy by determining the percentage of pitches above or below average they miscall by zone (based on pitchFX gameday zones 1 through 14) and the difference vs. the league average of umpires.

Data used for this model can be retrieved from the following kaggle page: [MLB Pitch Data 2015-2018](https://www.kaggle.com/pschale/mlb-pitch-data-20152018), additionally the data should be available to scrape through the R mlbgameday package.

<br>

**Data**

The data input files for this model were too large to upload but as mentioned can be found at the kaggle link above for download.

In the data directory you will find output files from the R code and corresponding analysis. 

<br>

**Metrics**

Several metrics were developed to measure the accuracy of each individual umpire’s pitch calling abilities, including an attempt at an all-encompassing stat to measure their effectiveness. These metrics are described below in further detail.


*Error Rate (ERR) & Adjusted Error Rate (ERR+)*:

Error rate consists strictly of an umpire’s incorrect calls divided by the total number of pitches they have called. The ERR+ metric adjusts each umpire’s error rate based on the league average error rate. This is calculated by taking each umpire’s ERR and multiplying it by the difference of their ERR vs the league average ERR.

*Error Rate Differential by Zone (ERDZ) & (ERDZ 100)*:

The error rate differential by zone of an umpire is calculated as the summation of each of their zone error rates vs the league average error rate of those zones. The ERDZ 100 version of this metric standardizes the stat to a level of 100 so it is easier to read (a number below 100 implies less errors while a number over 100 implies more errors than the league average).

*Home Plate Umpire Pitch Calling Accuracy Rating System (HARPER)*:

The HARPER metric creates an all-encompassing stat to measure umpire effectiveness based on a combination of the ERR+ and ERDZ 100 stats. This metric adds together the individual pitch calling tendencies of each zone of an umpire while considering the league average error rate of those zones.

<br>

**Umpire Classification**

Two techniques were used to classify umpires as either pitcher-friendly or hitter-friendly based on their pitch calling tendencies. In both cases, the variables used to calculate these classifications was based on each umpires: walk rate (walks per at-bat), strikeout rate (strikeouts per at-bat), homerun rate (homeruns per at-bat), runs per game (the combined runs of both teams per game), the inside zone error rate (pitches inside the strike zone that were incorrectly called balls based on total pitches called inside), and the outside zone error rate (pitches outside the strike zone that were incorrectly called strikes based on total pitches called outside).

<br>

**Model Output Example**

The below image shows an example of the model output using one umpire's statistics.

![](umpire_rating_system.png)